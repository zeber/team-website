export interface Blocks {
  type: 'blocks';
  id: string;
  attributes: object;
}

export interface MemberCardBlock {
  title: string;
  description: string;
  link: string;
  text: string;
}


export interface ImageUrl {
  w200: string;
  w400: string;
  w1080: string;
  w1920: string;
  w2560: string;
}

export interface MemberCard {
  block: MemberCardBlock;
  imageUrl: ImageUrl;
}

export interface MemberCards {
  [intex: string]: MemberCard;
}

export interface MemberCardsBlock {
  memberCards: MemberCards;
  title: string;
}
export interface MemberCardBlocks extends Blocks{
  attributes: MemberCardsBlock;
}
