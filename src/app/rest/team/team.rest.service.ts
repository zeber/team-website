import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { MemberCardBlocks, MemberCardsBlock } from './team.model';

@Injectable()
export class TeamRestService {
  constructor(private http: HttpClient) { }

  getTeamMembers(): Observable<MemberCardsBlock> {
    return this.http.get('https://cobiro-website-builder.s3-eu-west-1.amazonaws.com/task/index.json')
      .pipe(map((response: { data: [MemberCardBlocks] }) => response.data[0].attributes ));
  }
}
