import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHomeModule } from './page-home/page-home.module';



@NgModule({
  imports: [
    CommonModule,
    PageHomeModule
  ]
})
export class PagesModule { }
