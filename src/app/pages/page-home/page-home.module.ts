import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHomeComponent } from './page-home.component';
import { Routes, RouterModule } from '@angular/router';
import { TeamModule } from '../../views/team/team.module';

const routes: Routes = [{path: '', component: PageHomeComponent}];

@NgModule({
  declarations: [
    PageHomeComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TeamModule
  ]
})
export class PageHomeModule { }
