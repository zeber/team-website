import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamComponent } from './team.component';
import { HttpClientModule } from '@angular/common/http';
import { SpecialistCardModule } from './components/specialist-card/specialist-card.module';

@NgModule({
  declarations: [
    TeamComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    SpecialistCardModule
  ],
  exports: [
    TeamComponent
  ]
})
export class TeamModule { }
