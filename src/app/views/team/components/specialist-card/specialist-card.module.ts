import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecialistCardComponent } from './specialist-card.component';

@NgModule({
  declarations: [
    SpecialistCardComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SpecialistCardComponent
  ]
})
export class SpecialistCardModule { }
